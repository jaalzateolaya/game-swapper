/**
 * Back End File data interface.
 */
export interface BackEndFileData{
    name: string;
    url: string;
}
