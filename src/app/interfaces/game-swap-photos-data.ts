import { GameSwap } from '@core/modules/game-swaps/models/game-swap';
import { BackEndDataModel } from './back-end-data-model';
import { BackEndFileData } from './back-end-file-data';

export interface GameSwapPhotoData extends BackEndDataModel {
    data: BackEndFileData;
    gameSwap: GameSwap;
}
