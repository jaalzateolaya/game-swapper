import { BackEndDataModel } from './back-end-data-model';

/**
 * City data interface.
 */
export interface CityData extends BackEndDataModel {
    name: string;
}
