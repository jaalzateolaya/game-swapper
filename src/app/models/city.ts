import { CityData } from '@core/interfaces/city-data';
import { FrontEndModel } from './front-end-model';

/**
 * City model.
 */
export class City extends FrontEndModel implements CityData  {
    public name: string;

    public constructor(entity: CityData) {
        super(entity);
        this.name = entity.name;
    }
}
