import { BackEndFileData } from '@core/interfaces/back-end-file-data';
import { GameSwapPhotoData } from '@core/interfaces/game-swap-photos-data';
import { GameSwap } from '@core/modules/game-swaps/models/game-swap';
import { FrontEndModel } from './front-end-model';

export class GameSwapPhoto extends FrontEndModel implements GameSwapPhotoData {
    public data: BackEndFileData;
    public gameSwap: GameSwap;

    public constructor(entity: GameSwapPhotoData){
        super(entity);
        this.data = this.data;
    }
}
