import { asyncData } from 'tests/helpers';
import { API_BASE, API_HEADERS } from '@env/environment';
import { ConsolesService } from './consoles.service';
import { consolesMock, consoleMock } from 'tests/mocks/modules/consoles';
import { Console } from '../models/console';


describe('ConsolesService', () => {
    let httpClientSpy: { get: jasmine.Spy };
    let service: ConsolesService;

    const className = 'Console';
    const APIEndpoint = API_BASE + '/classes/' + className;

    beforeEach(() => {
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get']);
        service = new ConsolesService(httpClientSpy as any);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should be able to access to a specific console', async () => {
        const objectId = '121212121212121212121212';
        const endpoint = APIEndpoint + '/' + objectId;
        const mock = Object.assign({objectId}, consoleMock) as any;

        httpClientSpy.get.and.returnValue(asyncData(mock));

        const returnedObject = await service.getOne(objectId);

        expect(returnedObject).toBeInstanceOf(Console);
        expect(returnedObject).toEqual(jasmine.objectContaining(mock));
        expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
        expect(httpClientSpy.get.calls.first().args[0]).toEqual(endpoint);
    });

    it('should load all consoles', async () => {
        const consolesResults = {
            results: consolesMock,
        };
        const endpoint = APIEndpoint;

        httpClientSpy.get.and.returnValue(asyncData(consolesResults));

        const returnedObjects = await service.getAll();

        returnedObjects.forEach((element, i) => {
            expect(element).toBeInstanceOf(Console);
            expect(element).toEqual(jasmine.objectContaining(consolesMock[i]));
        });
        expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
        expect(httpClientSpy.get.calls.first().args[0]).toEqual(endpoint);
    });
});
