import { Injectable } from '@angular/core';

import { DataAPIService } from '@core/services/data-api.service';
import { Console } from '../models/console';

/**
 * Consoles REST client service.
 */
@Injectable({
  providedIn: 'root'
})
export class ConsolesService extends DataAPIService<Console> {
  public get className(): string{
    return 'Console';
  }

  public modelConstructor<ConsoleData>(data) {
    return new Console(data);
  }

  public handleRequestError() {
    // TODO: Implement me
    return Promise.reject('false');
  }

  public beforeSave(data) {
    return Promise.resolve(data);
  }
}
