import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { ConsoleData } from '../../interfaces/console-data';

/**
 * Console Card component to be used inside lists.
 */
@Component({
  selector: 'app-console-card',
  templateUrl: './console-card.component.html',
  styleUrls: ['./console-card.component.scss'],
})
export class ConsoleCardComponent implements OnInit {
    /**
     * The console to be shown.
     */
    @Input()
    public console: ConsoleData;

    /**
     * Console click event emitter.
     *
     * Triggered when the user clicks on any part of the card.
     */
    @Output()
    public consoleClick: EventEmitter<ConsoleData> = new EventEmitter();

    public constructor() {
    }

    public ngOnInit() {
    }

    /**
     * Console click event emitter.
     */
    public onClick() {
        this.consoleClick.emit(this.console);
    }
}
