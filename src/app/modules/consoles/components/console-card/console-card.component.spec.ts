import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConsoleCardComponent } from './console-card.component';
import { consoleMock } from 'tests/mocks/modules/consoles';

describe('ConsoleCardComponent', () => {
    let component: ConsoleCardComponent;
    let fixture: ComponentFixture<ConsoleCardComponent>;


    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ConsoleCardComponent],
            imports: [IonicModule.forRoot()]
        }).compileComponents();

        fixture = TestBed.createComponent(ConsoleCardComponent);
        component = fixture.componentInstance;
        component.console = consoleMock;

        fixture.detectChanges();
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('shold emit a console when clicked', () => {
        const card = fixture.nativeElement.querySelector('ion-card');
        const consoleClickEmmiter = spyOn(component.consoleClick, 'emit');

        card.click();

        expect(consoleClickEmmiter).toHaveBeenCalledTimes(1);
        expect(consoleClickEmmiter).toHaveBeenCalledWith(consoleMock);
    });

    it('should show an image pointing to the `ConsoleData::image` URL', () => {
        const image = fixture.nativeElement.querySelector('img');

        expect(image.src).toBe(consoleMock.image);
    });

    it('shold show the console title', () => {
        expect(fixture.nativeElement.textContent).toContain(consoleMock.title);
    });
});
