import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ConsoleCardComponent } from '../console-card/console-card.component';
import { ConsolesListComponent } from './consoles-list.component';
import { consolesMock } from 'tests/mocks/modules/consoles';

describe('ConsolesListComponent', () => {
    let component: ConsolesListComponent;
    let fixture: ComponentFixture<ConsolesListComponent>;


    beforeEach(async(() => {
        TestBed.configureTestingModule({
            declarations: [ ConsolesListComponent ],
            imports: [IonicModule.forRoot()]
        }).compileComponents();

        fixture = TestBed.createComponent(ConsolesListComponent);
        component = fixture.componentInstance;
        component.consoles = consolesMock;

        fixture.detectChanges();
    }));

    it('should create', () => {
        expect(component).toBeTruthy();
    });

    it('should show as many consoles as received', () => {
        const consolesEls = fixture.nativeElement.querySelectorAll('app-console-card');

        expect(consolesEls.length).toBe(consolesMock.length);
    });
});
