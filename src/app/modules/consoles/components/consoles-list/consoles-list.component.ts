import { Component, OnInit, Input } from '@angular/core';

import { ConsoleData } from '../../interfaces/console-data';

/**
 * Consoles list component to show consoles cards.
 */
@Component({
    selector: 'app-consoles-list',
    templateUrl: './consoles-list.component.html',
    styleUrls: ['./consoles-list.component.scss'],
})
export class ConsolesListComponent implements OnInit {
    /**
     * Consoles to be shown.
     */
    @Input()
    public consoles: ConsoleData[];

    public constructor() {}

    public ngOnInit() {}
}
