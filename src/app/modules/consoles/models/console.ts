import { ConsoleData } from '../interfaces/console-data';
import { FrontEndModel } from '@core/models/front-end-model';

/**
 * Console model.
 */
export class Console extends FrontEndModel implements ConsoleData {
    public title: string;
    public image: string;
    public color: number;

    public constructor(entity: ConsoleData) {
        super(entity);
        this.title = entity.title;
        this.image = entity.image;
        this.color = entity.color;
    }
}
