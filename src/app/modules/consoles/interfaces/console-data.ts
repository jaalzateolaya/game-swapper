import { BackEndDataModel } from '@core/interfaces/back-end-data-model';
/**
 * Console data interface.
 */
export interface ConsoleData extends BackEndDataModel {
    title: string;
    image: string;
    color: number;
}
