import { GameData } from '../interfaces/game-data';
import { FrontEndModel } from '@core/models/front-end-model';
import { Console } from '@core/modules/consoles/models/console';

/**
 * Game model.
 */
export class Game extends FrontEndModel implements GameData  {
    public title: string;
    public image: string;
    public console: Console;

    public get consoleTitle() {
        return this.console.title;
    }

    public get consoleColor() {
        return this.console.color;
    }

    public constructor(entity: GameData) {
        super(entity);
        this.title = entity.title;
        this.image = entity.image;
        this.console = entity.console;
    }
}
