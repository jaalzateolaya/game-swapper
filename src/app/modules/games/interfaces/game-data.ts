import { BackEndDataModel } from '@core/interfaces/back-end-data-model';
import { Console } from '@core/modules/consoles/models/console';

/**
 * Game data interface.
 */
export interface GameData extends BackEndDataModel {
    title: string;
    image: string;
    console: Console;
    readonly consoleColor: number;
    readonly consoleTitle: string;
}
