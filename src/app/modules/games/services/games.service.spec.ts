import { TestBed } from '@angular/core/testing';

import { GamesService } from './games.service';
import { API_BASE } from '@env/environment';
import { asyncData } from 'tests/helpers';
import { gameMock, gamesMock } from 'tests/mocks/modules/games';
import { Game } from '../models/game';
import { GameData } from '../interfaces/game-data';

describe('GamesService', () => {
  let httpClientSpy: { post: jasmine.Spy, patch: jasmine.Spy, get: jasmine.Spy };
  let service: GamesService;

  const className = 'Game';
  const APIEndpoint = API_BASE + '/classes/' + className;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'patch', 'post']);
    service = new GamesService(httpClientSpy as any);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be able to access to a specific registry', async () => {
      const objectId = '121212121212121212121212';
      const endpoint = APIEndpoint + '/' + objectId;
      const mock = Object.assign({objectId}, gameMock) as GameData;

      httpClientSpy.get.and.returnValue(asyncData(mock));

      const returnedObject = await service.getOne(objectId);

      expect(returnedObject).toBeInstanceOf(Game);
      expect(returnedObject).toEqual(jasmine.objectContaining(mock));
      expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
      expect(httpClientSpy.get.calls.first().args[0]).toEqual(endpoint);
  });

  it('should be able to access to all registries', async () => {
      const gamesResults = {
        results: gamesMock,
      };
      const endpoint = APIEndpoint;

      httpClientSpy.get.and.returnValue(asyncData(gamesResults));

      const returnedObjects = await service.getAll();

      returnedObjects.forEach((element, i) => {
        expect(element).toBeInstanceOf(Game);
        expect(element).toEqual(jasmine.objectContaining(gamesMock[i]));
      });
      expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
      expect(httpClientSpy.get.calls.first().args[0]).toEqual(endpoint);
  });
});
