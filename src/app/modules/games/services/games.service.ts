import { Injectable } from '@angular/core';
import { DataAPIService } from '@core/services/data-api.service';
import { Game } from '@core/modules/games/models/game';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class GamesService extends DataAPIService<Game> {
  public get className(): string{
    return 'Game';
  }

  /**
   * modelConstructor
   */
  public modelConstructor<GameData>(data) {
    return new Game(data);
  }

  public handleRequestError() {
    // TODO: Implement me
    return Promise.reject('false');
  }

  public beforeSave(data) {
    return Promise.resolve(data);
  }

  public constructor(protected httpClient: HttpClient) {
    super(httpClient);
  }

}
