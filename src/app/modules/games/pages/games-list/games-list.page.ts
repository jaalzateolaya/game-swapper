import { Component, OnInit } from '@angular/core';

import { Game } from '../../models/game';
import { GamesService } from '../../services/games.service';
import { GameData } from '../../interfaces/game-data';

@Component({
  selector: 'app-games-list-page',
  templateUrl: './games-list.page.html',
  styleUrls: ['./games-list.page.scss'],
})
export class GamesListPage implements OnInit {
  private games: Game[];

  constructor(
    protected gamesService: GamesService
  ) {}

  ngOnInit() {
    this.loadGames();
  }

  private loadGames(){
    this.gamesService.getAll()
      .then(backGames => this.games = backGames)
      .catch(error => {});
  }

  /**
   * The method that starts the game swap process
   */
  public startGameSwap(game: Game){

  }
}
