import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GamesListPageRoutingModule } from './games-list-routing.module';

import { GamesListPage } from './games-list.page';
import { GamesListComponent } from '../../components/games-list/games-list.component';
import { EmptyDataComponent } from '@core/components/empty-data/empty-data.component';
import { GameCardComponent } from '../../components/game-card/game-card.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GamesListPageRoutingModule
  ],
  declarations: [
    GamesListPage,
    GamesListComponent,
    GameCardComponent,
    EmptyDataComponent,
  ]
})
export class GamesListPageModule {}
