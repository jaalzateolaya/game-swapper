import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { gamesMock } from '@tests/mocks/modules/games';

import { GamesListPage } from './games-list.page';
import { GamesService } from '../../services/games.service';
import { Game } from '../../models/game';
import { GamesListComponent } from '../../components/games-list/games-list.component';
import { EmptyDataComponent } from '@core/components/empty-data/empty-data.component';
import { GameCardComponent } from '../../components/game-card/game-card.component';
import { FrontEndModel } from '@core/models/front-end-model';

describe('GamesListPage', () => {
  let component: GamesListPage;
  let fixture: ComponentFixture<GamesListPage>;
  let gamesServiceMock: {
    getAll: jasmine.Spy,
    getOne: jasmine.Spy,
  };

  const modeledGamesMock = gamesMock.map (gameData => new Game(gameData));

  beforeEach(async(() => {
    gamesServiceMock = jasmine.createSpyObj('GamesService', ['getAll', 'getOne']);
    gamesServiceMock.getAll.and.returnValue(Promise.resolve(modeledGamesMock));
    TestBed.configureTestingModule({
      declarations: [
        GamesListPage,
        GamesListComponent,
        GameCardComponent,
        EmptyDataComponent,
      ],
      providers: [{provide: GamesService, useValue: gamesServiceMock}],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GamesListPage);
    component = fixture.componentInstance;

    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should handle clicks events', async () => {
    fixture.detectChanges();

    const card = fixture.nativeElement.querySelector('app-game-card');
    const startGameSwapSpy = spyOn(component, 'startGameSwap');

    card.click();

    expect(startGameSwapSpy).toHaveBeenCalledTimes(1);
    expect(startGameSwapSpy.calls.first().args[0]).toBeInstanceOf(Game);
  });

  it('should show the game swapping form', () => {});
});
