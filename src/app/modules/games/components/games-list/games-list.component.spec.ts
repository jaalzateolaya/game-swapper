import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GamesListComponent } from './games-list.component';
import { gamesMock, gameMock } from 'tests/mocks/modules/games';

describe('GamesListComponent', () => {
  let component: GamesListComponent;
  let fixture: ComponentFixture<GamesListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GamesListComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GamesListComponent);
    component = fixture.componentInstance;
    component.games = gamesMock;

    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show as many games as received', () => {
    const gamesEls = fixture.nativeElement.querySelectorAll('app-game-card');

    expect(gamesEls.length).toBe(gamesMock.length);
  });

  it('should emit a game when a GameCard is clicked', () => {
    const card = fixture.nativeElement.querySelector('app-game-card');
    const gameEmmiter = spyOn(component.gameClick, 'emit');

    card.click();

    expect(gameEmmiter).toHaveBeenCalledTimes(1);
    expect(gameEmmiter).toHaveBeenCalledWith(gameMock);
  });
});
