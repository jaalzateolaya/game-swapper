import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

import { GameData } from '../../interfaces/game-data';

@Component({
  selector: 'app-games-list',
  templateUrl: './games-list.component.html',
  styleUrls: ['./games-list.component.scss'],
})
export class GamesListComponent implements OnInit {
  /**
   * The games to be shown.
   */
  @Input()
  public games: GameData[];

  /**
   * Game click event emitter.
   *
   * Triggered when the user clicks on any part of the card.
   */
  @Output()
  public gameClick: EventEmitter<GameData> = new EventEmitter();

  constructor() { }

  ngOnInit() {}

  /**
   * emits a Game
   */
  public onClick(game: GameData) {
    this.gameClick.emit(game);
  }
}
