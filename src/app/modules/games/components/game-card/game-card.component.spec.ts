import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GameCardComponent } from './game-card.component';
import { gameMock } from 'tests/mocks/modules/games';

describe('GameCardComponent', () => {
  let component: GameCardComponent;
  let fixture: ComponentFixture<GameCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameCardComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GameCardComponent);
    component = fixture.componentInstance;
    component.game = gameMock;

    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show an image pointing to the `GameData::image` URL', () => {
    const image = fixture.nativeElement.querySelector('img');

    expect(image.src).toBe(gameMock.image);
  });

  it('should show the game title', () => {
      expect(fixture.nativeElement.textContent).toContain(gameMock.title);
  });

  it('should show the console title', () => {
    expect(fixture.nativeElement.textContent).toContain(gameMock.console.title);
  });

  it ('should has a custom background color', () => {
    const cssBackground = '--background: #' + gameMock.console.color.toString(16) + ';';

    expect(component.backgroundColor()).toEqual(cssBackground);
  });

});
