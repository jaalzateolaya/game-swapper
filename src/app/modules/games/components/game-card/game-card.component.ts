import { Component, OnInit, Input } from '@angular/core';

import { GameData } from '../../interfaces/game-data';

@Component({
  selector: 'app-game-card',
  templateUrl: './game-card.component.html',
  styleUrls: ['./game-card.component.scss'],
})
export class GameCardComponent implements OnInit {
  /**
   * The game to be shown.
   */
  @Input()
  public game: GameData;

  constructor() { }

  ngOnInit() {}

  public backgroundColor(): string {
    return '--background: ' + this.hexColor() + ';';
  }

  private hexColor(): string{
    return '#' + this.game.consoleColor.toString(16);
  }

}
