import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss'],
})
export class LoginFormComponent implements OnInit {
  @Output()
  public submitForm: EventEmitter<{username: string, password: string}> = new EventEmitter();

  public loginForm: FormGroup;

  public constructor(
    private formBuilder: FormBuilder,
    private toastController: ToastController,
  ) {
    this.loginForm = this.formBuilder.group({
      username: ['', [Validators.email, Validators.required]],
      password: ['', Validators.required],
    });
  }

  ngOnInit() {}

  public async onSubmit(): Promise<void> {
    if (!this.loginForm.valid){
      if (this.loginForm.controls.password.hasError('required')){
        await this.handlePasswordAbsent();
        return;
      } else {
        await this.handleUsernameMisspelled();
        return;
      }
    }

    const value = this.loginForm.value;

    this.submitForm.emit(value);
  }

  public async handleUsernameMisspelled() {
    const toast = await this.toastController.create({
      message: 'Nombre de usuario debe ser un correo.',
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  public async handlePasswordAbsent() {
    const toast = await this.toastController.create({
      message: 'Debe escribir una contraseña.',
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }
}
