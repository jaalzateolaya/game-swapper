import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';

import { LoginFormComponent } from './login-form.component';

describe('LoginFormComponent', () => {
  let component: LoginFormComponent;
  let fixture: ComponentFixture<LoginFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginFormComponent ],
      imports: [
        IonicModule.forRoot(),
        ReactiveFormsModule,
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be false when form is empty', () => {
    expect(component.loginForm.valid).toBeFalsy();
  });

  it('should be false when username is not an email', () => {
    const username = component.loginForm.controls.username;
    const password = component.loginForm.controls.password;

    username.setValue('someuserame');
    password.setValue('somepassword');

    expect(component.loginForm.valid).toBeFalsy();
  });

  it('should be false when there is no password', () => {
    const username = component.loginForm.controls.username;
    const password = component.loginForm.controls.password;

    username.setValue('some@email.com');
    password.setValue('');

    expect(component.loginForm.valid).toBeFalsy();
  });

  it('should be truth when there is an username as email and a password', () => {
    const username = component.loginForm.controls.username;
    const password = component.loginForm.controls.password;

    username.setValue('some@email.com');
    password.setValue('somepassword');

    expect(component.loginForm.valid).toBeTruthy();
  });

  it('should emit login information when submit button is clicked', () => {
    const button = fixture.nativeElement.querySelector('ion-button');
    const loginFormEmmiter = spyOn(component.submitForm, 'emit');

    const username = component.loginForm.controls.username;
    const password = component.loginForm.controls.password;

    const loginFormMock = {
      username: 'some@email.com',
      password: 'somepassword',
    };

    username.setValue(loginFormMock.username);
    password.setValue(loginFormMock.password);

    button.click();

    expect(loginFormEmmiter).toHaveBeenCalledTimes(1);
    expect(loginFormEmmiter).toHaveBeenCalledWith(loginFormMock);
  });

  it('should show a toast when there is no password', () => {
    const handlePasswordSpy = spyOn(component, 'handlePasswordAbsent');
    const handleUsernameSpy = spyOn(component, 'handleUsernameMisspelled');
    const button = fixture.nativeElement.querySelector('ion-button');

    const username = component.loginForm.controls.username;
    const password = component.loginForm.controls.password;

    username.setValue('some@email.com');
    password.setValue('');

    button.click();

    expect(handlePasswordSpy).toHaveBeenCalledTimes(1);
    expect(handleUsernameSpy).toHaveBeenCalledTimes(0);
  });

  it('should show a toast when userame is not an email', () => {
    const handlePasswordSpy = spyOn(component, 'handlePasswordAbsent');
    const handleUsernameSpy = spyOn(component, 'handleUsernameMisspelled');
    const button = fixture.nativeElement.querySelector('ion-button');

    const username = component.loginForm.controls.username;
    const password = component.loginForm.controls.password;

    username.setValue('');
    password.setValue('somepassword');

    button.click();

    expect(handlePasswordSpy).toHaveBeenCalledTimes(0);
    expect(handleUsernameSpy).toHaveBeenCalledTimes(1);
  });
});
