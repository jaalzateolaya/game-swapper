import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { CityData } from '@core/interfaces/city-data';
import { City } from '@core/models/city';
import { ToastController } from '@ionic/angular';

@Component({
  selector: 'app-signup-form',
  templateUrl: './signup-form.component.html',
  styleUrls: ['./signup-form.component.scss'],
})
export class SignupFormComponent implements OnInit {
  /**
   * The cities to be in the form.
   */
  @Input()
  public cities: CityData[];

  /**
   * Submit form event emitter.
   *
   * Triggered when the user clicks the submit button.
   */
  @Output()
  public submitForm: EventEmitter<{
    username: string,
    password: string,
    repassword: string,
    forename: string,
    lastname: string,
    address: string,
    city: City,
    mobile: string,
  }> = new EventEmitter();

  public signupForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private toastController: ToastController,
  ) {
    this.signupForm = this.formBuilder.group({
      username: ['', [Validators.email, Validators.required]],
      password: ['', Validators.required],
      repassword: ['', Validators.required],
      forename: ['', Validators.required],
      lastname: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      mobile: ['', [Validators.pattern('^[0-9]*$'), Validators.required]]
    });
  }

  ngOnInit() {}

  public onSubmit(): void{
    if (!this.signupForm.valid){
      this.handleInvalidForm();
      return;
    }
    if (this.signupForm.value.password !== this.signupForm.value.repassword){
      this.handleInvalidPasswords();
      return;
    }

    const value = this.signupForm.value;

    this.submitForm.emit(value);
  }

  public async handleInvalidForm() {
    const toast = await this.toastController.create({
      message: 'Información incorrecta.',
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  public async handleInvalidPasswords() {
    const toast = await this.toastController.create({
      message: 'Contraseñas no coinciden.',
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }
}
