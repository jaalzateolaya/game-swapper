import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { citiesMock, cityMock } from 'tests/mocks/modules/cities';

import { SignupFormComponent } from './signup-form.component';

describe('SignupFormComponent', () => {
  let component: SignupFormComponent;
  let fixture: ComponentFixture<SignupFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SignupFormComponent ],
      imports: [
        IonicModule.forRoot(),
        ReactiveFormsModule,
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(SignupFormComponent);
    component = fixture.componentInstance;
    component.cities = citiesMock;

    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be invalid when form is empty', () => {
    expect(component.signupForm.valid).toBeFalsy();
  });

  it('should be invalid when username is not an email', () => {
    const username = component.signupForm.controls.username;
    const password = component.signupForm.controls.password;
    const repassword = component.signupForm.controls.repassword;
    const forename = component.signupForm.controls.forename;
    const lastname = component.signupForm.controls.lastname;
    const address = component.signupForm.controls.address;
    const city = component.signupForm.controls.city;
    const mobile = component.signupForm.controls.mobile;

    username.setValue('someusername');
    password.setValue('somepassword');
    repassword.setValue('somepassword');
    forename.setValue('Pedro');
    lastname.setValue('Navaja');
    address.setValue('someaddress');
    city.setValue(cityMock);
    mobile.setValue('12345');

    expect(component.signupForm.valid).toBeFalsy();
  });

  it('should be invalid when there is no password', () => {
    const username = component.signupForm.controls.username;
    const password = component.signupForm.controls.password;
    const repassword = component.signupForm.controls.repassword;
    const forename = component.signupForm.controls.forename;
    const lastname = component.signupForm.controls.lastname;
    const address = component.signupForm.controls.address;
    const city = component.signupForm.controls.city;
    const mobile = component.signupForm.controls.mobile;

    username.setValue('someemail@email.com');
    password.setValue('');
    repassword.setValue('somepassword');
    forename.setValue('Pedro');
    lastname.setValue('Navaja');
    address.setValue('someaddress');
    city.setValue(cityMock);
    mobile.setValue('12345');

    expect(component.signupForm.valid).toBeFalsy();
  });

  it('should be invalid when there is no repassword', () => {
    const username = component.signupForm.controls.username;
    const password = component.signupForm.controls.password;
    const repassword = component.signupForm.controls.repassword;
    const forename = component.signupForm.controls.forename;
    const lastname = component.signupForm.controls.lastname;
    const address = component.signupForm.controls.address;
    const city = component.signupForm.controls.city;
    const mobile = component.signupForm.controls.mobile;

    username.setValue('someemail@email.com');
    password.setValue('somepassword');
    repassword.setValue('');
    forename.setValue('Pedro');
    lastname.setValue('Navaja');
    address.setValue('someaddress');
    city.setValue(cityMock);
    mobile.setValue('12345');

    expect(component.signupForm.valid).toBeFalsy();
  });

  it('should be invalid when there is no forename', () => {
    const username = component.signupForm.controls.username;
    const password = component.signupForm.controls.password;
    const repassword = component.signupForm.controls.repassword;
    const forename = component.signupForm.controls.forename;
    const lastname = component.signupForm.controls.lastname;
    const address = component.signupForm.controls.address;
    const city = component.signupForm.controls.city;
    const mobile = component.signupForm.controls.mobile;

    username.setValue('someemail@email.com');
    password.setValue('somepassword');
    repassword.setValue('somepassword');
    forename.setValue('');
    lastname.setValue('Navaja');
    address.setValue('someaddress');
    city.setValue(cityMock);
    mobile.setValue('12345');

    expect(component.signupForm.valid).toBeFalsy();
  });

  it('should be invalid when there is no lastname', () => {
    const username = component.signupForm.controls.username;
    const password = component.signupForm.controls.password;
    const repassword = component.signupForm.controls.repassword;
    const forename = component.signupForm.controls.forename;
    const lastname = component.signupForm.controls.lastname;
    const address = component.signupForm.controls.address;
    const city = component.signupForm.controls.city;
    const mobile = component.signupForm.controls.mobile;

    username.setValue('someemail@email.com');
    password.setValue('somepassword');
    repassword.setValue('somepassword');
    forename.setValue('Pedro');
    lastname.setValue('');
    address.setValue('someaddress');
    city.setValue(cityMock);
    mobile.setValue('12345');

    expect(component.signupForm.valid).toBeFalsy();
  });

  it('should be invalid when there is no address', () => {
    const username = component.signupForm.controls.username;
    const password = component.signupForm.controls.password;
    const repassword = component.signupForm.controls.repassword;
    const forename = component.signupForm.controls.forename;
    const lastname = component.signupForm.controls.lastname;
    const address = component.signupForm.controls.address;
    const city = component.signupForm.controls.city;
    const mobile = component.signupForm.controls.mobile;

    username.setValue('someemail@email.com');
    password.setValue('somepassword');
    repassword.setValue('somepassword');
    forename.setValue('Pedro');
    lastname.setValue('Navaja');
    address.setValue('');
    address.setValue(cityMock);
    mobile.setValue('12345');

    expect(component.signupForm.valid).toBeFalsy();
  });

  it('should be invalid when there is no city', () => {
    const username = component.signupForm.controls.username;
    const password = component.signupForm.controls.password;
    const repassword = component.signupForm.controls.repassword;
    const forename = component.signupForm.controls.forename;
    const lastname = component.signupForm.controls.lastname;
    const address = component.signupForm.controls.address;
    const city = component.signupForm.controls.city;
    const mobile = component.signupForm.controls.mobile;

    username.setValue('someemail@email.com');
    password.setValue('somepassword');
    repassword.setValue('somepassword');
    forename.setValue('Pedro');
    lastname.setValue('Navaja');
    address.setValue('someaddress');
    city.setValue('');
    mobile.setValue('12345');

    expect(component.signupForm.valid).toBeFalsy();
  });

  it('should be invalid when there is no mobile number', () => {
    const username = component.signupForm.controls.username;
    const password = component.signupForm.controls.password;
    const repassword = component.signupForm.controls.repassword;
    const forename = component.signupForm.controls.forename;
    const lastname = component.signupForm.controls.lastname;
    const address = component.signupForm.controls.address;
    const city = component.signupForm.controls.city;
    const mobile = component.signupForm.controls.mobile;

    username.setValue('someemail@email.com');
    password.setValue('somepassword');
    repassword.setValue('somepassword');
    forename.setValue('Pedro');
    lastname.setValue('Navaja');
    address.setValue('someaddress');
    city.setValue(cityMock);
    mobile.setValue('');

    expect(component.signupForm.valid).toBeFalsy();
  });

  it('should be invalid when mobile is not a number', () => {
    const username = component.signupForm.controls.username;
    const password = component.signupForm.controls.password;
    const repassword = component.signupForm.controls.repassword;
    const forename = component.signupForm.controls.forename;
    const lastname = component.signupForm.controls.lastname;
    const address = component.signupForm.controls.address;
    const city = component.signupForm.controls.city;
    const mobile = component.signupForm.controls.mobile;

    username.setValue('someemail@email.com');
    password.setValue('somepassword');
    repassword.setValue('somepassword');
    forename.setValue('Pedro');
    lastname.setValue('Navaja');
    address.setValue('someaddress');
    city.setValue(cityMock);
    mobile.setValue('hola');

    expect(component.signupForm.valid).toBeFalsy();
  });

  it('should be valid when the form is correctly filled', () => {
    const username = component.signupForm.controls.username;
    const password = component.signupForm.controls.password;
    const repassword = component.signupForm.controls.repassword;
    const forename = component.signupForm.controls.forename;
    const lastname = component.signupForm.controls.lastname;
    const address = component.signupForm.controls.address;
    const city = component.signupForm.controls.city;
    const mobile = component.signupForm.controls.mobile;

    username.setValue('someemail@email.com');
    password.setValue('somepassword');
    repassword.setValue('somepassword');
    forename.setValue('Pedro');
    lastname.setValue('Navaja');
    address.setValue('someaddress');
    city.setValue(cityMock);
    mobile.setValue('12345');

    expect(component.signupForm.valid).toBeTruthy();
  });

  it('should emit information when submit button is clicked and form correctly filled', () => {
    const button = fixture.nativeElement.querySelector('ion-button');
    const signupFormEmmiter = spyOn(component.submitForm, 'emit');

    const username = component.signupForm.controls.username;
    const password = component.signupForm.controls.password;
    const repassword = component.signupForm.controls.repassword;
    const forename = component.signupForm.controls.forename;
    const lastname = component.signupForm.controls.lastname;
    const address = component.signupForm.controls.address;
    const city = component.signupForm.controls.city;
    const mobile = component.signupForm.controls.mobile;

    const signupFormMock = {
      username: 'some@email.com',
      password: 'somepassword',
      repassword: 'somepassword',
      forename: 'Pedro',
      lastname: 'Navaja',
      address: 'someaddress',
      city: cityMock,
      mobile: '12345'
    };

    username.setValue(signupFormMock.username);
    password.setValue(signupFormMock.password);
    repassword.setValue(signupFormMock.repassword);
    forename.setValue(signupFormMock.forename);
    lastname.setValue(signupFormMock.lastname);
    address.setValue(signupFormMock.address);
    city.setValue(signupFormMock.city);
    mobile.setValue(signupFormMock.mobile);

    button.click();

    expect(signupFormEmmiter).toHaveBeenCalledTimes(1);
    expect(signupFormEmmiter).toHaveBeenCalledWith(signupFormMock);
  });

  it('should not emit information when submit button is clicked and passwords do not match', () => {
    const button = fixture.nativeElement.querySelector('ion-button');
    const signupFormEmmiter = spyOn(component.submitForm, 'emit');

    const username = component.signupForm.controls.username;
    const password = component.signupForm.controls.password;
    const repassword = component.signupForm.controls.repassword;
    const forename = component.signupForm.controls.forename;
    const lastname = component.signupForm.controls.lastname;
    const address = component.signupForm.controls.address;
    const city = component.signupForm.controls.city;
    const mobile = component.signupForm.controls.mobile;

    const signupFormMock = {
      username: 'some@email.com',
      password: 'somepassword',
      repassword: 'someotherpassword',
      forename: 'Pedro',
      lastname: 'Navaja',
      address: 'someaddress',
      city: cityMock,
      mobile: '12345'
    };

    username.setValue(signupFormMock.username);
    password.setValue(signupFormMock.password);
    repassword.setValue(signupFormMock.repassword);
    forename.setValue(signupFormMock.forename);
    lastname.setValue(signupFormMock.lastname);
    address.setValue(signupFormMock.address);
    city.setValue(signupFormMock.city);
    mobile.setValue(signupFormMock.mobile);

    button.click();

    expect(signupFormEmmiter).toHaveBeenCalledTimes(0);
  });

  it('should show a toast when there password and repassword are not the same', () => {
    const handleInvalidFormSpy = spyOn(component, 'handleInvalidForm');
    const handleInvalidPasswordsSpy = spyOn(component, 'handleInvalidPasswords');
    const button = fixture.nativeElement.querySelector('ion-button');

    const username = component.signupForm.controls.username;
    const password = component.signupForm.controls.password;
    const repassword = component.signupForm.controls.repassword;
    const forename = component.signupForm.controls.forename;
    const lastname = component.signupForm.controls.lastname;
    const address = component.signupForm.controls.address;
    const city = component.signupForm.controls.city;
    const mobile = component.signupForm.controls.mobile;

    username.setValue('someemail@email.com');
    password.setValue('somepassword');
    repassword.setValue('somepassword2');
    forename.setValue('Pedro');
    lastname.setValue('Navaja');
    address.setValue('someaddress');
    city.setValue(cityMock),
    mobile.setValue('12345');

    button.click();

    expect(handleInvalidPasswordsSpy).toHaveBeenCalledTimes(1);
    expect(handleInvalidFormSpy).toHaveBeenCalledTimes(0);
  });

  it('should show a toast when password and repassword are not the same', () => {
    const handleInvalidFormSpy = spyOn(component, 'handleInvalidForm');
    const handleInvalidPasswordsSpy = spyOn(component, 'handleInvalidPasswords');
    const button = fixture.nativeElement.querySelector('ion-button');

    const username = component.signupForm.controls.username;
    const password = component.signupForm.controls.password;
    const repassword = component.signupForm.controls.repassword;
    const forename = component.signupForm.controls.forename;
    const lastname = component.signupForm.controls.lastname;
    const address = component.signupForm.controls.address;
    const city = component.signupForm.controls.city;
    const mobile = component.signupForm.controls.mobile;

    username.setValue('someemail@email.com');
    password.setValue('somepassword');
    repassword.setValue('somepassword');
    forename.setValue('');
    lastname.setValue('Navaja');
    address.setValue('someaddress');
    city.setValue(cityMock);
    mobile.setValue('12345');

    button.click();

    expect(handleInvalidPasswordsSpy).toHaveBeenCalledTimes(0);
    expect(handleInvalidFormSpy).toHaveBeenCalledTimes(1);
  });

});
