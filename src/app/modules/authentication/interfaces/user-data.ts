import { BackEndDataModel } from '@core/interfaces/back-end-data-model';
import { Person } from '@core/modules/persons/models/person';

export interface UserData extends BackEndDataModel {
    username: string;
    password: string;
    person: Person;
}
