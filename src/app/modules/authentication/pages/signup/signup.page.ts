import { Component, OnInit } from '@angular/core';
import { ModalController, ToastController } from '@ionic/angular';

import { City } from '@core/models/city';
import { Person } from '@core/modules/persons/models/person';
import { CitiesService } from '@core/services/cities.service';
import { User } from '../../models/user';
import { SignupService } from '../../services/signup.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.page.html',
  styleUrls: ['./signup.page.scss'],
})
export class SignupPage implements OnInit {
  public cities: City[];

  constructor(
    protected citiesService: CitiesService,
    protected signupService: SignupService,
    private toastController: ToastController,
    private modalController: ModalController,
    ) { }

  ngOnInit() {
    this.loadCities();
  }

  private loadCities(): void{
    this.citiesService.getAll()
      .then(backCities => this.cities = backCities);
  }

  public async handleHttpError(errorResponse) {
    let message: string;

    if (errorResponse.error.error){
      message = errorResponse.error.error;
    } else {
      message = 'Cannot connect to server';
    }

    const toast = await this.toastController.create({
      message,
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  public async successSignUp(){
    const toast = await this.toastController.create({
      message: 'Registrado exitosamente',
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  public async onSignUpUser(signUpData) {
    const person = new Person({
      forename: signUpData.forename,
      lastname: signUpData.lastname,
      address: signUpData.address,
      mobile: signUpData.mobile,
      city: signUpData.city,
    });

    const user = new User({
      username: signUpData.username,
      password: signUpData.password,
      person,
    });

    try {
      await this.signupService.signUp(user);
      this.successSignUp();
    } catch (error) {
      this.handleHttpError(error);
      throw error;
    }
  }

  public goBack() {
    this.modalController.dismiss();
  }
}
