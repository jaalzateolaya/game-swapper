import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule, ToastController } from '@ionic/angular';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';

import { City } from '@core/models/city';
import { citiesMock, cityMock } from 'tests/mocks/modules/cities';
import { SignupPage } from './signup.page';
import { CitiesService } from '@core/services/cities.service';
import { SignupService } from '../../services/signup.service';
import { SignupFormComponent } from '../../components/signup-form/signup-form.component';

describe('SignupPage', () => {
  let component: SignupPage;
  let fixture: ComponentFixture<SignupPage>;
  let citiesServiceMock: {
    getAll: jasmine.Spy,
  };
  let signupServiceMock: {
    signUp: jasmine.Spy,
  };
  let toastControllerMock: {
    create: jasmine.Spy,
  };

  const formMock = {
    username: 'user@mock.com',
    password: 'passwordmock',
    repassword: 'passwordmock',
    forename: 'Pepito',
    lastname: 'Perez',
    address: 'Mz busquela Casa encuentrela',
    mobile: '1234',
    city: cityMock,
  };

  const modeledCitiesMock = citiesMock.map (cityData => new City(cityData));

  beforeEach(async(() => {
    citiesServiceMock = jasmine.createSpyObj('CitiesService', ['getAll']);
    citiesServiceMock.getAll.and.returnValue(Promise.resolve(modeledCitiesMock));

    signupServiceMock = jasmine.createSpyObj('SignupService', ['signUp']);

    toastControllerMock = jasmine.createSpyObj('ToastController', ['create']);
    toastControllerMock.create.and.returnValue({present: () => undefined});

    TestBed.configureTestingModule({
      declarations: [
        SignupPage,
        SignupFormComponent ],
      providers: [
        {provide: CitiesService, useValue: citiesServiceMock},
        {provide: SignupService, useValue: signupServiceMock},
        {provide: ToastController, useValue: toastControllerMock},
      ],
      imports: [
        IonicModule.forRoot(),
        ReactiveFormsModule,
        RouterTestingModule,
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(SignupPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should dismiss modal when cancel button is clicked', () => {
    const button = fixture.nativeElement.querySelector('ion-button[type=\'button\']');
    const goBackSpy = spyOn(component, 'goBack');

    button.click();

    expect(goBackSpy).toHaveBeenCalledTimes(1);
  });

  it('should try a signup', async () => {
    await component.onSignUpUser(formMock);

    expect(signupServiceMock.signUp).toHaveBeenCalledTimes(1);
  });

  it('should show a success toast when user is succesfully signed up', async () => {
    const handleHttpErrorSpy = spyOn(component, 'handleHttpError');
    const successSignUpSpy = spyOn(component, 'successSignUp');

    signupServiceMock.signUp.and.returnValue(Promise.resolve(true));

    await component.onSignUpUser(formMock);

    expect(handleHttpErrorSpy).toHaveBeenCalledTimes(0);
    expect(successSignUpSpy).toHaveBeenCalledTimes(1);
  });

  it('should show an error toast when user is not succesfully signed up', async () => {
    const handleHttpErrorSpy = spyOn(component, 'handleHttpError');
    const successSignUpSpy = spyOn(component, 'successSignUp');

    signupServiceMock.signUp.and.returnValue(Promise.reject());

    try {
      await component.onSignUpUser(formMock);
    } catch (error) {
      expect(handleHttpErrorSpy).toHaveBeenCalledTimes(1);
      expect(successSignUpSpy).toHaveBeenCalledTimes(0);
    }
  });
});
