import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginPageRoutingModule } from './login-routing.module';

import { LoginPage } from './login.page';
import { LoginFormComponent } from '../../components/login-form/login-form.component';
import { SignupPage } from '../signup/signup.page';
import { SignupFormComponent } from '../../components/signup-form/signup-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    LoginPageRoutingModule
  ],
  declarations: [
    LoginPage,
    LoginFormComponent,
    SignupPage,
    SignupFormComponent,
  ],
  entryComponents: [
    SignupPage
  ]
})
export class LoginPageModule {}
