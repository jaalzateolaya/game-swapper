import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { IonicModule } from '@ionic/angular';

import { LoginService } from '../../services/login.service';
import { LoginPage } from './login.page';

describe('LoginPage', () => {
  let component: LoginPage;
  let fixture: ComponentFixture<LoginPage>;
  let loginServiceMock: {
    logIn: jasmine.Spy,
  };
  const formMock = {
    username: 'fulanito@detal.com',
    password: 'detallomejor',
  };

  beforeEach(async(() => {
    loginServiceMock = jasmine.createSpyObj('LoginService', ['logIn']);

    TestBed.configureTestingModule({
      declarations: [ LoginPage ],
      providers: [
        {provide: LoginService, useValue: loginServiceMock},
      ],
      imports: [
        IonicModule.forRoot(),
        ReactiveFormsModule,
        RouterTestingModule,
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should try a login', async () => {
    await component.onLogin(formMock);

    expect(loginServiceMock.logIn).toHaveBeenCalledTimes(1);
  });

  it('should emit an event when user is succesfully logged', async () => {
    const eventEmitter = spyOn(component.isLogged, 'emit');

    await component.onLogin(formMock);

    expect(eventEmitter).toHaveBeenCalledTimes(1);
  });

  it('should show a modal when sign up invitation is clicked', () => {
    const button = fixture.nativeElement.querySelector('ion-text');
    const goToSignUpSpy = spyOn(component, 'goToSignUpForm');

    button.click();

    expect(goToSignUpSpy).toHaveBeenCalledTimes(1);
  });

  it('should show a toast when there is an non-101 error in log in', async () => {
    const handleErrorSpy = spyOn(component, 'handleError');
    const handleInvalidLoginInfoSpy = spyOn(component, 'handleInvalidLoginInfo');

    const error = {
      error: {
        code: 610,
      }
    };

    loginServiceMock.logIn.and.returnValue(Promise.reject(error));

    await component.onLogin(formMock);

    expect(handleErrorSpy).toHaveBeenCalledTimes(1);
    expect(handleInvalidLoginInfoSpy).toHaveBeenCalledTimes(0);
  });

  it('should show a toast when there is an error associated to username or password', async () => {
    const handleErrorSpy = spyOn(component, 'handleError');
    const handleInvalidLoginInfoSpy = spyOn(component, 'handleInvalidLoginInfo');

    const error = {
      error: {
        code: 101,
      }
    };

    loginServiceMock.logIn.and.returnValue(Promise.reject(error));

    await component.onLogin(formMock);

    expect(handleErrorSpy).toHaveBeenCalledTimes(0);
    expect(handleInvalidLoginInfoSpy).toHaveBeenCalledTimes(1);
  });
});
