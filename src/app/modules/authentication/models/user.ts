import { FrontEndModel } from '@core/models/front-end-model';
import { Person } from '@core/modules/persons/models/person';
import { UserData } from '../interfaces/user-data';

export class User extends FrontEndModel implements UserData {
    public username: string;
    public password: string;
    public person: Person;

    public constructor(entity: UserData){
        super(entity);
        this.username = entity.username;
        this.password = entity.password;
        this.person = entity.person;
    }
}
