import { TestBed } from '@angular/core/testing';

import { Person } from '@core/modules/persons/models/person';
import { API_BASE, API_HEADERS } from '@env/environment';
import { asyncData } from 'tests/helpers';
import { personMock } from 'tests/mocks/modules/persons';
import { UserData } from '../interfaces/user-data';
import { User } from '../models/user';
import { SignupService } from './signup.service';

describe('SignupService', () => {
  let service: SignupService;
  let httpClientSpy: {
    get: jasmine.Spy,
    patch: jasmine.Spy,
    post: jasmine.Spy
  };
  let personsServiceSpy: {
    createPerson: jasmine.Spy,
  };

  beforeEach(() => {
    TestBed.configureTestingModule({});
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'patch', 'post']);
    personsServiceSpy = jasmine.createSpyObj('PersonsService', ['createPerson']);
    service = new SignupService(httpClientSpy as any, personsServiceSpy as any);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should signup an user', async () => {
    const username = 'cooldude6@cooldomain.com';
    const password = 'S0meÑP@ssword';

    const person = new Person(personMock);

    const userMock = {
      username,
      password,
      person,
    } as UserData;

    const backendUserMock = {
      username: userMock.username,
      password: userMock.password,
      person: {
        __type: 'Pointer',
        className: 'Person',
        objectId: '1a1a1a1a1a',
      },
    };

    const endpoint = API_BASE + '/users';
    const headers = {
      headers: API_HEADERS,
    };

    personsServiceSpy.createPerson.and.returnValue({objectId: '1a1a1a1a1a'});
    httpClientSpy.post.and.returnValue(asyncData(({objectId: '1b1b1b1b1b'})));

    const returnedObject = await service.signUp(new User (userMock));

    expect(returnedObject).toEqual({objectId: '1b1b1b1b1b'});
    expect(httpClientSpy.post).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.post.calls.first().args[0]).toEqual(endpoint);
    expect(httpClientSpy.post.calls.first().args[1]).toEqual(backendUserMock);
    expect(httpClientSpy.post.calls.first().args[2]).toEqual(headers);
  });
});
