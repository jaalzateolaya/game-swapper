import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { City } from '@core/models/city';
import { Person } from '@core/modules/persons/models/person';
import { PersonsService } from '@core/modules/persons/services/persons.service';
import { CitiesService } from '@core/services/cities.service';

import { API_BASE, API_HEADERS } from '@env/environment';
import { SessionData } from '../interfaces/session-data';
import { AuthenticationService } from './authentication.service';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    protected httpClient: HttpClient,
    private authentication: AuthenticationService,
    private personsService: PersonsService,
    private citiesService: CitiesService,
    ) { }

  /**
   * Allow an user to login in the app
   */
  public logIn(username: string, password: string): Promise<SessionData> {
    const APIEndPoint = API_BASE + '/login';
    const params = {
      username,
      password
    };
    const options = {
      headers: API_HEADERS,
      params,
    };

    let session: SessionData;

    return this.httpClient.get<SessionData>(APIEndPoint, options)
      .toPromise()
      .then(loadedSession => {
        session = loadedSession;

        return this.personsService.getOne(session.person.objectId);
      }).then(person => {
        session.person = person;

        return this.citiesService.getOne(person.city.objectId);
      }).then(city => {
        session.person.city = city;
        this.authentication.saveSession(session);
        return session;
      });

  }

  /**
   * Allow an user to log out
   */
  public logOut() {
    const sessionToken = this.authentication.sessionToken;
    const APIEndPoint = API_BASE + '/logout';
    const sessionTokenHeader = {
      'X-Parse-Session-Token' : sessionToken,
    };
    const headers = Object.assign(sessionTokenHeader, API_HEADERS);
    const options = {
      headers
    };

    this.httpClient.post(APIEndPoint, null, options);

    this.authentication.cleanSession();
  }
}
