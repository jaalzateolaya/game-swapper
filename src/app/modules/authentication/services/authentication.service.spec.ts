import { TestBed } from '@angular/core/testing';
import { cityMock } from 'tests/mocks/modules/cities';
import { personMock, personsMock } from 'tests/mocks/modules/persons';

import { AuthenticationService } from './authentication.service';

describe('AuthenticationService', () => {
  let service: AuthenticationService;
  let personsServiceSpy: {
    getOne: jasmine.Spy,
  };
  let citiesServiceSpy: {
    getOne: jasmine.Spy,
  };

  const loginMock = {
    username: 'cooldude6@cooldomain.com',
    person: personMock,
    objectId: 'g7y9tkhB7O',
    sessionToken: 'r:pnktnjyb996sj4p156gjtp4im',
  };

  const alternativeLoginMock = {
    username: 'cooldude6@cooldomain.com',
    person: personsMock[1],
    objectId: 'g7y9tkhB7O',
    sessionToken: 'r:pnktnjyb996sj4p156gjtp4im',
  };

  beforeEach(() => {
    TestBed.configureTestingModule({});
    personsServiceSpy = jasmine.createSpyObj('PersonsService', ['getOne']);
    citiesServiceSpy = jasmine.createSpyObj('CitiesService', ['getOne']);
    service = new AuthenticationService(
      personsServiceSpy as any,
      citiesServiceSpy as any,
    );
    localStorage.clear();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should retrieve session information from local storage', () => {
    service.saveSession(loginMock);

    const session = service.getSession();

    expect(session).toEqual(loginMock);
  });

  it('should return false when session is not started', () => {
    const isSessionStarted = service.isAuthenticated;

    expect(isSessionStarted).toBeFalse();
  });

  it('should return true when session is not started', () => {
    service.saveSession(loginMock);

    const isSessionStarted = service.isAuthenticated;

    expect(isSessionStarted).toBeTrue();
  });

  it('should return the user id', () => {
    service.saveSession(loginMock);

    const userId = service.userId;

    expect(userId).toEqual(loginMock.objectId);
  });

  it('should return the username', () => {
    service.saveSession(loginMock);

    const username = service.username;

    expect(username).toEqual(loginMock.username);
  });

  it('should update the session information', async () => {
    service.saveSession(alternativeLoginMock);

    personsServiceSpy.getOne.and.returnValue(Promise.resolve(personMock));
    citiesServiceSpy.getOne.and.returnValue(Promise.resolve(cityMock));

    await service.updateSession();

    const session = service.getSession();

    expect(session).toEqual(loginMock);
    expect(personsServiceSpy.getOne).toHaveBeenCalledTimes(1);
    expect(citiesServiceSpy.getOne).toHaveBeenCalledTimes(1);
  });
});
