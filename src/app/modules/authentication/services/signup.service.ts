import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { PersonsService } from '@core/modules/persons/services/persons.service';
import { API_BASE, API_HEADERS } from '@env/environment';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  constructor(
    protected httpClient: HttpClient,
    protected personsService: PersonsService,
  ) { }

  /**
   * Allow an user to sign up
   */
  public async signUp(user: User): Promise<any>{
    const APIEndPointUser = API_BASE + '/users';
    const APIHeader = {
      headers: API_HEADERS,
    };

    const response = await this.personsService.createPerson(user.person);
    const backendUser = {
      username: user.username,
      password: user.password,
      person: {
        __type: 'Pointer',
        className: 'Person',
        objectId: response.objectId,
      },
    };

    return this.httpClient.post(APIEndPointUser, backendUser, APIHeader).toPromise();
  }
}
