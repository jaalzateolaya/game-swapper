import { Component, OnInit } from '@angular/core';

import { City } from '@core/models/city';
import { SessionData } from '@core/modules/authentication/interfaces/session-data';
import { AuthenticationService } from '@core/modules/authentication/services/authentication.service';
import { CitiesService } from '@core/services/cities.service';
import { Person } from '../../models/person';
import { PersonsService } from '../../services/persons.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {
  public session: SessionData;
  public cities: City[];

  constructor(
    protected authenticationService: AuthenticationService,
    protected citiesService: CitiesService,
    protected personsService: PersonsService,
  ) { }

  ngOnInit() {
    this.loadInformation();
  }

  public loadInformation(): void{
    if (this.authenticationService.isAuthenticated){
      this.session = this.authenticationService.getSession();
      this.loadCities();
    }
  }

  public onModifyPerson(person: Person): void{
    // TODO: Session should be updated with the person modifications
    const newPerson = new Person(person);
    this.personsService.save(newPerson);
  }

  private loadCities(): void{
    this.citiesService.getAll()
      .then(backCities => this.cities = backCities);
  }

}
