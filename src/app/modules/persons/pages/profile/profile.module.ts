import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfilePageRoutingModule } from './profile-routing.module';

import { LoginFormComponent } from '@core/modules/authentication/components/login-form/login-form.component';
import { LoginPage } from '@core/modules/authentication/pages/login/login.page';
import { ProfileFormComponent } from '../../components/profile-form/profile-form.component';
import { ProfilePage } from './profile.page';
import { SignupPage } from '@core/modules/authentication/pages/signup/signup.page';
import { SignupFormComponent } from '@core/modules/authentication/components/signup-form/signup-form.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    ProfilePageRoutingModule
  ],
  declarations: [ProfilePage, LoginPage, LoginFormComponent, ProfileFormComponent, SignupPage, SignupFormComponent]
})
export class ProfilePageModule {}
