import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { City } from '@core/models/city';
import { AuthenticationService } from '@core/modules/authentication/services/authentication.service';
import { CitiesService } from '@core/services/cities.service';
import { citiesMock } from 'tests/mocks/modules/cities';
import { personMock } from 'tests/mocks/modules/persons';

import { PersonsService } from '../../services/persons.service';
import { ProfilePage } from './profile.page';

describe('ProfilePage', () => {
  let component: ProfilePage;
  let fixture: ComponentFixture<ProfilePage>;
  let citiesServiceMock: {
    getAll: jasmine.Spy,
  };
  let authenticationServiceMock: {
    getSession: jasmine.Spy,
    isAuthenticated: jasmine.SpyObjPropertyNames,
  };
  let personsServiceMock: {
    save: jasmine.Spy,
  };

  const modeledCitiesMock = citiesMock.map (cityData => new City(cityData));
  const loginMock = {
    username: 'cooldude6@cooldomain.com',
    person: personMock,
    objectId: 'g7y9tkhB7O',
    sessionToken: 'r:pnktnjyb996sj4p156gjtp4im',
  };

  beforeEach(async(() => {
    citiesServiceMock = jasmine.createSpyObj('CitiesService', ['getAll']);
    citiesServiceMock.getAll.and.returnValue(Promise.resolve(modeledCitiesMock));

    personsServiceMock = jasmine.createSpyObj('PersonsService', ['save']);
  }));

  it('should create when session is not started', () => {
    authenticationServiceMock = jasmine.createSpyObj('AuthenticationService', ['getSession'], {isAuthenticated: false});

    TestBed.configureTestingModule({
      declarations: [ ProfilePage ],
      providers: [
        {provide: CitiesService, useValue: citiesServiceMock},
        {provide: AuthenticationService, useValue: authenticationServiceMock},
        {provide: PersonsService, useValue: personsServiceMock},
      ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(component).toBeTruthy();
  });

  it('should create when session is started', () => {
    authenticationServiceMock = jasmine.createSpyObj('AuthenticationService', ['getSession'], {isAuthenticated: true});
    authenticationServiceMock.getSession.and.returnValue(loginMock);

    TestBed.configureTestingModule({
      declarations: [ ProfilePage ],
      providers: [
        {provide: CitiesService, useValue: citiesServiceMock},
        {provide: AuthenticationService, useValue: authenticationServiceMock},
        {provide: PersonsService, useValue: personsServiceMock},
      ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(component).toBeTruthy();
  });

  it('should show login page if session is not started', () => {
    authenticationServiceMock = jasmine.createSpyObj('AuthenticationService', ['getSession'], {isAuthenticated: false});

    TestBed.configureTestingModule({
      declarations: [ ProfilePage ],
      providers: [
        {provide: CitiesService, useValue: citiesServiceMock},
        {provide: AuthenticationService, useValue: authenticationServiceMock},
        {provide: PersonsService, useValue: personsServiceMock},
      ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const loginPage = fixture.nativeElement.querySelector('app-login');
    const profileForm = fixture.nativeElement.querySelector('app-profile-form');

    expect(loginPage).toBeTruthy();
    expect(profileForm).toBeFalsy();
  });

  it('should show profile form if session is started', () => {
    authenticationServiceMock = jasmine.createSpyObj('AuthenticationService', ['getSession'], {isAuthenticated: true});
    authenticationServiceMock.getSession.and.returnValue(loginMock);

    TestBed.configureTestingModule({
      declarations: [ ProfilePage ],
      providers: [
        {provide: CitiesService, useValue: citiesServiceMock},
        {provide: AuthenticationService, useValue: authenticationServiceMock},
        {provide: PersonsService, useValue: personsServiceMock},
      ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();

    const loginPage = fixture.nativeElement.querySelector('app-login');
    const profileForm = fixture.nativeElement.querySelector('app-profile-form');

    expect(loginPage).toBeFalsy();
    expect(profileForm).toBeTruthy();
  });

  it('should load the user information when session is started', () => {
    authenticationServiceMock = jasmine.createSpyObj('AuthenticationService', ['getSession'], {isAuthenticated: true});
    authenticationServiceMock.getSession.and.returnValue(loginMock);

    TestBed.configureTestingModule({
      declarations: [ ProfilePage ],
      providers: [
        {provide: CitiesService, useValue: citiesServiceMock},
        {provide: AuthenticationService, useValue: authenticationServiceMock},
        {provide: PersonsService, useValue: personsServiceMock},
      ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();

    expect(authenticationServiceMock.getSession).toHaveBeenCalledTimes(1);
    expect(citiesServiceMock.getAll).toHaveBeenCalledTimes(1);
  });

  it('should send modified person to `PersonsService` to be saved in database', () => {
    authenticationServiceMock = jasmine.createSpyObj('AuthenticationService', ['getSession'], {isAuthenticated: true});
    authenticationServiceMock.getSession.and.returnValue(loginMock);

    TestBed.configureTestingModule({
      declarations: [ ProfilePage ],
      providers: [
        {provide: CitiesService, useValue: citiesServiceMock},
        {provide: AuthenticationService, useValue: authenticationServiceMock},
        {provide: PersonsService, useValue: personsServiceMock},
      ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProfilePage);
    component = fixture.componentInstance;
    fixture.detectChanges();

    component.onModifyPerson(personMock);

    expect(personsServiceMock.save).toHaveBeenCalledTimes(1);
  });
});
