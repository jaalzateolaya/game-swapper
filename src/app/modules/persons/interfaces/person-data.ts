import { BackEndDataModel } from '@core/interfaces/back-end-data-model';
import { City } from '@core/models/city';

export interface PersonData extends BackEndDataModel {
    forename: string;
    lastname: string;
    address: string;
    mobile: string;
    city: City;
}
