import { API_BASE, API_HEADERS } from '@env/environment';
import { asyncData } from 'tests/helpers';
import { personMock } from 'tests/mocks/modules/persons';
import { PersonData } from '../interfaces/person-data';
import { Person } from '../models/person';
import { PersonsService } from './persons.service';

describe('PersonsService', () => {
  let httpClientSpy: {post: jasmine.Spy, put: jasmine.Spy, get: jasmine.Spy};
  let service: PersonsService;

  const className = 'Person';
  const APIEndpoint = API_BASE + '/classes/' + className;
  const APIHeaders = API_HEADERS;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post', 'put']);
    service = new PersonsService(httpClientSpy as any);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be able to access  to specific registry', async () => {
    const objectId = '121212121212121212121212';
    const endpoint = APIEndpoint + '/' + objectId;
    const mock = Object.assign({objectId}, personMock) as PersonData;

    httpClientSpy.get.and.returnValue(asyncData(mock));

    const returnObject = await service.getOne(objectId);

    expect(returnObject).toBeInstanceOf(Person);
    expect(returnObject).toEqual(jasmine.objectContaining(mock));
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.get.calls.first().args[0]).toEqual(endpoint);
  });

  it('should be able to edit the information of an existent person', async () => {
    let existentObject = Object.assign({
      objectId: '121212121212121212121212',
      }, personMock) as Person;
    existentObject = new Person(existentObject);
    const endpoint = APIEndpoint + '/' + existentObject.id;

    httpClientSpy.put.and.returnValue(asyncData(existentObject));

    const returnedObject = await service.save(existentObject);

    expect(returnedObject).toEqual(existentObject);
    expect(httpClientSpy.post).toHaveBeenCalledTimes(0);
    expect(httpClientSpy.put).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.put.calls.first().args[0]).toEqual(endpoint);
    expect(httpClientSpy.put.calls.first().args[1]).toEqual(personMock);
    expect(httpClientSpy.put.calls.first().args[2]).toEqual({headers: APIHeaders});

  });

  it('should create a Person with a pointer to city', async () => {
    const person = new Person(personMock);
    const backendPerson = {
      forename: person.forename,
      lastname: person.lastname,
      address: person.address,
      mobile: person.mobile,
      city: {
        __type: 'Pointer',
        className: 'City',
        objectId: person.city.id,
      },
    };
    const endpoint = API_BASE + '/classes/Person';

    httpClientSpy.post.and.returnValue(asyncData({objectId: '1a1a1a1a1a'}));

    const returnedObject = await service.createPerson(person);

    expect(returnedObject).toEqual({objectId: '1a1a1a1a1a'});
    expect(httpClientSpy.post.calls.first().args[0]).toEqual(endpoint);
    expect(httpClientSpy.post.calls.first().args[1]).toEqual(backendPerson);
    expect(httpClientSpy.post.calls.first().args[2]).toEqual({headers: APIHeaders});
  });
});
