import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { DataAPIService } from '@core/services/data-api.service';
import { API_BASE, API_HEADERS } from '@env/environment';
import { PersonData } from '../interfaces/person-data';
import { Person } from '../models/person';

@Injectable({
  providedIn: 'root'
})
export class PersonsService extends DataAPIService<Person> {
  public get className(): string{
    return 'Person';
  }

  public modelConstructor<PersonData>(data) {
    return new Person(data);
  }

  public handleRequestError(){
    return Promise.reject('false');
  }

  public beforeSave(data){
    return Promise.resolve(data);
  }

  /**
   * Create a Person with a pointer to City
   */
  public createPerson(person: PersonData){
    const APIEndPoint = API_BASE + '/classes/Person';
    const APIHeader = {
      headers: API_HEADERS,
    };

    const backendPerson = {
      forename: person.forename,
      lastname: person.lastname,
      address: person.address,
      mobile: person.mobile,
      city: {
        __type: 'Pointer',
        className: 'City',
        objectId: person.city.id,
      },
    };

    return this.httpClient.post<{objectId}>(APIEndPoint, backendPerson, APIHeader)
            .toPromise();
  }

  public constructor(protected httpClient: HttpClient){
    super(httpClient);
  }
}
