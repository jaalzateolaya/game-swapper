import { FrontEndModel } from '@core/models/front-end-model';
import { PersonData } from '../interfaces/person-data';
import { City } from '@core/models/city';

export class Person extends FrontEndModel implements PersonData {
    public forename: string;
    public lastname: string;
    public address: string;
    public mobile: string;
    public city: City;

    public get fullname() {
        return this.forename + ' ' + this.lastname;
    }

    public constructor(entity: PersonData){
        super(entity);
        this.forename = entity.forename;
        this.lastname = entity.lastname;
        this.address = entity.address;
        this.mobile = entity.mobile;
        this.city = entity.city;
    }
}
