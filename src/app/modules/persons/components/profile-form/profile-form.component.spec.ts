import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { personMock } from 'tests/mocks/modules/persons';

import { ProfileFormComponent } from './profile-form.component';

describe('ProfileFormComponent', () => {
  let component: ProfileFormComponent;
  let fixture: ComponentFixture<ProfileFormComponent>;
  let personMockWithId = personMock;

  personMockWithId = Object.assign({
    id: '1a1a1a1a1a',
    }, personMockWithId);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfileFormComponent ],
      imports: [IonicModule.forRoot(),
                ReactiveFormsModule]
    }).compileComponents();

    fixture = TestBed.createComponent(ProfileFormComponent);
    component = fixture.componentInstance;
    component.personData = personMockWithId;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be false when the form it`s empty', () => {
    const forename = component.personForm.controls.forename;
    const lastname = component.personForm.controls.lastname;
    const address = component.personForm.controls.address;
    const city = component.personForm.controls.city;
    const mobile = component.personForm.controls.mobile;

    forename.setValue('');
    lastname.setValue('');
    address.setValue('');
    city.setValue('');
    mobile.setValue('');

    expect(component.personForm.valid).toBeFalsy();
  });

  it('should show a toast when form is false', () => {
    const handleInvalidFormSpy = spyOn(component, 'handleInvalidForm');
    const button = fixture.nativeElement.querySelector('#sendButton');

    const forename = component.personForm.controls.forename;
    const lastname = component.personForm.controls.lastname;
    const address = component.personForm.controls.address;
    const city = component.personForm.controls.city;
    const mobile = component.personForm.controls.mobile;

    forename.setValue('');
    lastname.setValue('');
    address.setValue('');
    city.setValue('');
    mobile.setValue('asd');

    button.click();

    expect(handleInvalidFormSpy).toHaveBeenCalledTimes(1);
  });

  it ('should show `Person` information in the form', () => {
    const spyForm = component.personForm.value;

    expect(spyForm.forename).toEqual(personMock.forename);
    expect(spyForm.lastname).toEqual(personMock.lastname);
    expect(spyForm.address).toEqual(personMock.address);
    expect(spyForm.city).toEqual(personMock.city);
    expect(spyForm.mobile).toEqual(personMock.mobile);
  });

  it('should disabled the form by default', () => {
    expect(component.personForm.status).toEqual('DISABLED');
  });

  it('should enable the form when edit button is clicked', () => {
    const button = fixture.nativeElement.querySelector('#editButton');
    const editSpy = spyOn(component, 'enableForm');

    button.click();

    expect(editSpy).toHaveBeenCalledTimes(1);
  });

  it('should emit a form', () => {
    const button = fixture.nativeElement.querySelector('#sendButton');
    const personFormEmmiter = spyOn(component.submitForm, 'emit');
    const personMockWithObjectId = Object.assign({
      objectId: personMockWithId.id,
      }, personMock);

    component.personForm.enable();

    const forename = component.personForm.controls.forename;
    const lastname = component.personForm.controls.lastname;
    const address = component.personForm.controls.address;
    const city = component.personForm.controls.city;
    const mobile = component.personForm.controls.mobile;

    forename.setValue(personMock.forename);
    lastname.setValue(personMock.lastname);
    address.setValue(personMock.address);
    city.setValue(personMock.city);
    mobile.setValue(personMock.mobile);


    button.click();

    expect(personFormEmmiter).toHaveBeenCalledTimes(1);
    expect(personFormEmmiter).toHaveBeenCalledWith(personMockWithObjectId);
  });
});
