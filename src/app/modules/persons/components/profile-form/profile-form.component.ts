import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';

import { City } from '@core/models/city';
import { Person } from '../../models/person';

@Component({
  selector: 'app-profile-form',
  templateUrl: './profile-form.component.html',
  styleUrls: ['./profile-form.component.scss'],
})
export class ProfileFormComponent implements OnInit {
  @Input()
  public personData: Person;
  @Input()
  public cities: City[];

  @Output()
  public submitForm: EventEmitter<Person> = new EventEmitter();

  public personForm: FormGroup;

  public constructor(
    private formBuilder: FormBuilder,
    private toastController: ToastController,
  ) {
    this.personForm = this.formBuilder.group({
      forename: ['', Validators.required],
      lastname: ['', Validators.required],
      address: ['', Validators.required],
      city: ['', Validators.required],
      mobile: ['', [Validators.pattern('^[0-9]*$'), Validators.required]],
    });
   }

  ngOnInit() {
    if (this.personData) {
      this.personForm.setValue({
        forename: this.personData.forename,
        lastname: this.personData.lastname,
        address: this.personData.address,
        city: this.personData.city,
        mobile: this.personData.mobile,
      });
      this.personForm.disable();
    }
  }

  public async handleInvalidForm() {
    const toast = await this.toastController.create({
      message: 'Información incorrecta.',
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  public onSubmit(): Promise<void>{
    let value = this.personForm.value;

    if (!this.personForm.valid){
      this.handleInvalidForm();
      return;
    }

    value = Object.assign({
      objectId: this.personData.id,
      }, value);

    this.submitForm.emit(value);
  }

  /**
   * Enable the inputs of the form
   */
  public enableForm(): void{
    this.personForm.enable();
  }

}
