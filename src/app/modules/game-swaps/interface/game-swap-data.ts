import { BackEndDataModel } from '@core/interfaces/back-end-data-model';
import { GameSwapPhoto } from '@core/models/game-swap-photo';
import { User } from '@core/modules/authentication/models/user';
import { Console } from '@core/modules/consoles/models/console';
import { Game } from '@core/modules/games/models/game';

export enum GameSwapStatus{
    pendingForEmailValidation,
    pendingForApproval,
    approved,
    rejected,
    priceRejectedByUser,
    priceAccepted,
    canceledByUser
}
export enum GameSwapStatusLabels {
    pendingForEmailValidation = 'Pendiente por validación del correo electronico',
    pendingForApproval = 'pendiente por aprobación',
    approved = 'Aprobado',
    rejected = 'Rechazado',
    priceRejectedByUser = 'Precio rechazado por el usuario',
    priceAccepted = 'Precio aceptado',
    canceledByUser = 'Cancelado por el usuario'
}
export interface GameSwapData extends BackEndDataModel {
    gameTitle: string;
    gameDescription: string;
    gameConsole: Console;
    photos: GameSwapPhoto[];
    gameWished: Game;
    user: User;
    status: GameSwapStatus;
}
