import { Component, OnInit } from '@angular/core';

import { GameSwap } from '../../models/game-swap';
import { GameSwapsService } from '../../services/game-swaps.service';

@Component({
  selector: 'app-game-swaps-list-page',
  templateUrl: './game-swaps-list.page.html',
  styleUrls: ['./game-swaps-list.page.scss'],
})
export class GameSwapsListPage implements OnInit {
  public gameSwaps: GameSwap[];

  constructor(
    protected gameSwapsService: GameSwapsService
  ) { }

  ngOnInit() {
    this.gameSwaps = this.gameSwapsService.loadGameSwaps();
  }
}
