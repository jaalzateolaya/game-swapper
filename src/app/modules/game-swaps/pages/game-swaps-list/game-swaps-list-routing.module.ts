import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GameSwapsListPage } from './game-swaps-list.page';

const routes: Routes = [
  {
    path: '',
    component: GameSwapsListPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GameSwapsListPageRoutingModule {}
