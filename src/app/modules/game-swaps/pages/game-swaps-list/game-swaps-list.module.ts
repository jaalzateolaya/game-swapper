import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GameSwapsListPageRoutingModule } from './game-swaps-list-routing.module';

import { GameSwapsListPage } from './game-swaps-list.page';
import { GameSwapListComponent } from '../../components/game-swap-list/game-swap-list.component';
import { GameSwapCardComponent } from '../../components/game-swap-card/game-swap-card.component';
import { EmptyDataComponent } from '@core/components/empty-data/empty-data.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GameSwapsListPageRoutingModule
  ],
  declarations: [
    GameSwapsListPage,
    GameSwapListComponent,
    GameSwapCardComponent,
    EmptyDataComponent
  ]
})
export class GameSwapsListPageModule {}
