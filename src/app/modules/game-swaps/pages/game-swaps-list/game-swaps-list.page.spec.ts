import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { EmptyDataComponent } from '@core/components/empty-data/empty-data.component';
import { gameSwapsMock } from 'tests/mocks/modules/game-swap';
import { GameSwapCardComponent } from '../../components/game-swap-card/game-swap-card.component';
import { GameSwapListComponent } from '../../components/game-swap-list/game-swap-list.component';
import { GameSwapsService } from '../../services/game-swaps.service';
import { GameSwapsListPage } from './game-swaps-list.page';

describe('GameSwapsListPage', () => {
  let component: GameSwapsListPage;
  let fixture: ComponentFixture<GameSwapsListPage>;
  let gameSwapsServiceMock: {
    loadGameSwaps: jasmine.Spy
  };

  beforeEach(async(() => {
    gameSwapsServiceMock = jasmine.createSpyObj('GameSwapsService', ['loadGameSwaps']);
    gameSwapsServiceMock.loadGameSwaps.and.returnValue(gameSwapsMock);
    TestBed.configureTestingModule({
      declarations: [
        GameSwapsListPage,
        GameSwapListComponent,
        GameSwapCardComponent,
        EmptyDataComponent
      ],
      providers: [
      {
        provide: GameSwapsService,
        useValue: gameSwapsServiceMock
      }],
      imports: [IonicModule.forRoot()]
    }).compileComponents();
    fixture = TestBed.createComponent(GameSwapsListPage);
    component = fixture.componentInstance;

    fixture.detectChanges();
  }));

  it('should create', async () => {
    expect(component).toBeTruthy();
  });

  it('should show the same amount of `GameSwap` it loads', () => {
    const cards = fixture.nativeElement.querySelectorAll('app-game-swap-card');

    expect(cards.length).toEqual(gameSwapsMock.length);
  });
});
