import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { GameSwapCardComponent } from './game-swap-card.component';
import { gameSwapMock } from 'tests/mocks/modules/game-swap';

describe('GameSwapCardComponent', () => {
  let component: GameSwapCardComponent;
  let fixture: ComponentFixture<GameSwapCardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameSwapCardComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GameSwapCardComponent);
    component = fixture.componentInstance;
    component.gameSwap = gameSwapMock;

    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show game title', () => {
    expect(fixture.nativeElement.textContent).toContain(gameSwapMock.gameTitle);
  });

  it('should show game wished title ', () => {
    expect(fixture.nativeElement.textContent).toContain(gameSwapMock.gameWished.title);
  });

  it('should has a custom background color', () => {
    const cssBackground = '--background: #' + gameSwapMock.gameWished.console.color.toString(16) + ';';

    expect(component.backgroundColorCard()).toEqual(cssBackground);
  });

  it('Should has a custom background status color', () => {
    const cssBackground = '--background: #FFD700;';

    expect(component.backgroundColorStatus()).toEqual(cssBackground);
  });
});
