import { Component, Input, OnInit } from '@angular/core';
import { GameSwapData } from '../../interface/game-swap-data';

@Component({
  selector: 'app-game-swap-card',
  templateUrl: './game-swap-card.component.html',
  styleUrls: ['./game-swap-card.component.scss'],
})
export class GameSwapCardComponent implements OnInit {
  @Input()
  public gameSwap: GameSwapData;

  constructor() { }

  ngOnInit() {}

  public backgroundColorStatus(): string{
    return '--background: ' + this.colorCheck() + ';';
  }

  public colorCheck(): string{
    switch (this.gameSwap.status){
      case 0: {
        return '#FFD700';
      }
      case 1: {
        return '#FFD700';
      }
      case 2: {
        return '#ADFF2F';
      }
      case 3: {
        return 'FF4040';
      }
      case 4: {
        return 'FF4040';
      }
      case 5: {
        return '#ADFF2F';
      }
      case 6: {
        return 'FF4040';
      }
      default: {
        return '#FFD700';
      }
    }
  }

  public backgroundColorCard(): string{
    return '--background: ' + this.hexColor() + ';';
  }

  public hexColor(): string{
    return '#' + this.gameSwap.gameWished.consoleColor.toString(16);
  }

}
