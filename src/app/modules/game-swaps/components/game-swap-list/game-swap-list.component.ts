import { Component, Input, OnInit } from '@angular/core';
import { GameSwapData } from '@core/modules/game-swaps/interface/game-swap-data';

@Component({
  selector: 'app-game-swap-list',
  templateUrl: './game-swap-list.component.html',
  styleUrls: ['./game-swap-list.component.scss'],
})
export class GameSwapListComponent implements OnInit {
  @Input()
  public gameSwaps: GameSwapData[];

  constructor() { }

  ngOnInit() {}

}
