import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';
import { gameSwapsMock } from 'tests/mocks/modules/game-swap';

import { GameSwapListComponent } from './game-swap-list.component';

describe('GameSwapListComponent', () => {
  let component: GameSwapListComponent;
  let fixture: ComponentFixture<GameSwapListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameSwapListComponent ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(GameSwapListComponent);
    component = fixture.componentInstance;
    component.gameSwaps = gameSwapsMock;

    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should show as many game swap as received', () => {
    const gamesSwapList = fixture.nativeElement.querySelectorAll('app-game-swap-card');

    expect(gamesSwapList.length).toBe(gameSwapsMock.length);
  });
});
