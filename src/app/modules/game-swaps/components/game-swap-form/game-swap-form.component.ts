import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';

import { Console } from '@core/modules/consoles/models/console';

@Component({
  selector: 'app-game-swap-form',
  templateUrl: './game-swap-form.component.html',
  styleUrls: ['./game-swap-form.component.scss'],
})
export class GameSwapFormComponent implements OnInit {
  @Input()
  public gameConsole: Console[];

  @Output()
  public submitForm: EventEmitter<{
    gameTitle: string,
    gameDescription: string,
    gameConsole: Console
  }> = new EventEmitter();

  public gameSwapForm: FormGroup;

  constructor(
    private toastController: ToastController,
    private formBuilder: FormBuilder
    )
  {
    this.gameSwapForm = this.formBuilder.group({
      gameTitle: ['', Validators.required],
      gameDescription: ['', Validators.required],
      gameConsole: ['', Validators.required]
    });
  }

  ngOnInit() {}

  public async handleInvalidForm() {
    const toast = await this.toastController.create({
      message: 'Información incompleta.',
      position: 'bottom',
      duration: 2000
    });
    toast.present();
  }

  public onSubmit(): Promise<void> {
    const value = this.gameSwapForm.value;

    if (!this.gameSwapForm.valid){
      this.handleInvalidForm();
      return;
    }

    this.submitForm.emit(value);
  }

}
