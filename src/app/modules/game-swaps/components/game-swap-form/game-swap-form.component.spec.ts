import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { consolesMock } from 'tests/mocks/modules/consoles';
import { gameSwapMock } from 'tests/mocks/modules/game-swap';

import { GameSwapFormComponent } from './game-swap-form.component';

describe('GameSwapFormComponent', () => {
  let component: GameSwapFormComponent;
  let fixture: ComponentFixture<GameSwapFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GameSwapFormComponent ],
      imports: [IonicModule.forRoot(),
                ReactiveFormsModule]
    }).compileComponents();

    fixture = TestBed.createComponent(GameSwapFormComponent);
    component = fixture.componentInstance;
    component.gameConsole = consolesMock;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should be false when the form is empty', () => {
    const gameTitle = component.gameSwapForm.controls.gameTitle;
    const gameDescription = component.gameSwapForm.controls.gameDescription;
    const gameConsole = component.gameSwapForm.controls.gameConsole;

    gameTitle.setValue('');
    gameDescription.setValue('');
    gameConsole.setValue('');

    expect(component.gameSwapForm.valid).toBeFalsy();
  });

  it('should show toast when the form is false', () => {
    const handleInvalidFormSpy = spyOn(component, 'handleInvalidForm');
    const button = fixture.nativeElement.querySelector('#emitButton');

    const gameTitle = component.gameSwapForm.controls.gameTitle;
    const gameDescription = component.gameSwapForm.controls.gameDescription;
    const gameConsole = component.gameSwapForm.controls.gameConsole;

    gameTitle.setValue('');
    gameDescription.setValue('');
    gameConsole.setValue(null);

    button.click();

    expect(handleInvalidFormSpy).toHaveBeenCalledTimes(1);
  });

  it('should emit `GameSwap` information when the submit button is clicked', () => {
    const button = fixture.nativeElement.querySelector('#emitButton');
    const gameSwapFormEmmiter = spyOn(component.submitForm, 'emit');

    const gameSwapFormMock = {
      gameTitle: gameSwapMock.gameTitle,
      gameDescription: gameSwapMock.gameDescription,
      gameConsole: gameSwapMock.gameConsole
    };

    const gameTitle = component.gameSwapForm.controls.gameTitle;
    const gameDescription = component.gameSwapForm.controls.gameDescription;
    const gameConsole = component.gameSwapForm.controls.gameConsole;

    gameTitle.setValue(gameSwapFormMock.gameTitle);
    gameDescription.setValue(gameSwapFormMock.gameDescription);
    gameConsole.setValue(gameSwapFormMock.gameConsole);

    button.click();

    expect(gameSwapFormEmmiter).toHaveBeenCalledTimes(1);
    expect(gameSwapFormEmmiter).toHaveBeenCalledWith(gameSwapFormMock);
  });
});
