import { HttpParams } from '@angular/common/http';
import { UserData } from '@core/modules/authentication/interfaces/user-data';
import { API_BASE, API_HEADERS } from '@env/environment';
import { asyncData } from 'tests/helpers';
import { gameSwapMock, gameSwapsMock } from 'tests/mocks/modules/game-swap';
import { userMock } from 'tests/mocks/modules/user';
import { GameSwapData } from '../interface/game-swap-data';
import { GameSwap } from '../models/game-swap';

import { GameSwapsService } from './game-swaps.service';

describe('GameSwapsService', () => {
  let httpClientSpy: {
    post: jasmine.Spy,
    patch: jasmine.Spy,
    get: jasmine.Spy
  };
  let authenticationServiceSpy: {
    isAuthenticated: jasmine.SpyObjPropertyNames,
    userId: jasmine.SpyObjPropertyNames
  };
  let service: GameSwapsService;

  const className = 'GameSwap';
  const APIEndpoint = API_BASE + '/classes/' + className;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'patch', 'post']);
    authenticationServiceSpy = jasmine.createSpyObj('AuthenticationService', {isAuthenticated: false, userId: '12121212'});
    service = new GameSwapsService(httpClientSpy as any, authenticationServiceSpy as any);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should be able to access to a specific registry', async () => {
    const objectId = '121212121212121212121212';
    const endpoint = APIEndpoint + '/' + objectId;
    const mock = Object.assign({objectId}, gameSwapMock) as GameSwapData;

    httpClientSpy.get.and.returnValue(asyncData(mock));

    const returnedObject = await service.getOne(objectId);

    expect(returnedObject).toBeInstanceOf(GameSwap);
    expect(returnedObject).toEqual(jasmine.objectContaining(mock));
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.get.calls.first().args[0]).toEqual(endpoint);
  });

  it('should be able to access to all registries', async () => {
    const gamesResults = {
      results: gameSwapsMock,
    };
    const endpoint = APIEndpoint;

    httpClientSpy.get.and.returnValue(asyncData(gamesResults));

    const returnedObjects = await service.getAll();

    returnedObjects.forEach((element, i) => {
      expect(element).toBeInstanceOf(GameSwap);
      expect(element).toEqual(jasmine.objectContaining(gameSwapsMock[i]));
    });
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.get.calls.first().args[0]).toEqual(endpoint);
  });

  it('Should return `GameSwaps` related to the user', async () => {
    const objectId = '121212121212121212121212';
    const endpoint = APIEndpoint;
    const mock = Object.assign({objectId}, userMock) as UserData;
    const relation = {
      user: {
        __type: 'Pointer',
      className: 'User',
      objectId: mock.objectId
      }
    };
    const object = new HttpParams().set('where', JSON.stringify(relation));
    const headers = {
      headers: API_HEADERS,
      params: object
    };
    const gameSwapsResults = {
      results: gameSwapsMock
    };

    httpClientSpy.get.and.returnValue(asyncData(gameSwapsResults));

    const returnedObjects = await service.getRelate(objectId);

    returnedObjects.forEach((element, i) => {
      expect(element).toBeInstanceOf(GameSwap);
      expect(element).toEqual(jasmine.objectContaining(gameSwapsMock[i]));
    });
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.get.calls.first().args[0]).toEqual(endpoint);
    expect(httpClientSpy.get.calls.first().args[1]).toEqual(headers);
  });

  it('Should return `GameSwap`s related to user when the session is started', async () => {
    authenticationServiceSpy = jasmine.createSpyObj(
      'AuthenticationService',
      {},
      {isAuthenticated: true, userId: '121212'}
      );
    const gameSwapsResults = {
        results: gameSwapsMock
      };

    httpClientSpy.get.and.returnValue(asyncData(gameSwapsResults));

    const returnedObjects = await service.loadGameSwaps();

    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);

    returnedObjects.forEach((element, i) => {
      expect(element).toBeInstanceOf(GameSwap);
      expect(element).toEqual(jasmine.objectContaining(gameSwapsMock[i]));
    });
  });
});
