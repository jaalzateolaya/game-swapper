import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AuthenticationService } from '@core/modules/authentication/services/authentication.service';
import { DataAPIService } from '@core/services/data-api.service';
import { API_BASE, API_HEADERS } from '@env/environment';
import { GameSwap } from '../models/game-swap';

@Injectable({
  providedIn: 'root'
})
export class GameSwapsService extends DataAPIService<GameSwap> {
  constructor(
    protected httpClient: HttpClient,
    protected authenticationService: AuthenticationService) {
    super(httpClient);
  }

  public get className(): string{
    return 'GameSwap';
  }

  /**
   * modelConstructor
   */
  public modelConstructor<GameSwapData>(data) {
    return new GameSwap(data);
  }

  public handleRequestError() {
    return Promise.reject('false');
  }

  public beforeSave(data) {
    return Promise.resolve(data);
  }

  public getRelate(id: string): any {
    const relation = {
      user: {
        __type: 'Pointer',
        className: 'User',
        objectId: id
      }
    };
    const object = new HttpParams().set('where', JSON.stringify(relation));

    return this.httpClient.get<{results}>(this.APIEndpointt, {headers: API_HEADERS, params: object})
      .toPromise()
      .then(response => response.results)
      .then(models => {
        return models.map(model => this.modelConstructor(model));
        })
      .catch(this.handleRequestError);
  }
  public loadGameSwaps(): GameSwap[] {
    let gameSwaps: GameSwap[];
    if (this.authenticationService.isAuthenticated) {
       gameSwaps = this.getRelate(this.authenticationService.userId);
    }
    return gameSwaps;
  }

  private get APIEndpointt(): string{
    return API_BASE + '/classes/' + this.className;
  }
}
