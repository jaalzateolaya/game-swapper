import { FrontEndModel } from '@core/models/front-end-model';
import { GameSwapPhoto } from '@core/models/game-swap-photo';
import { User } from '@core/modules/authentication/models/user';
import { Console } from '@core/modules/consoles/models/console';
import { Game } from '@core/modules/games/models/game';
import { GameSwapData, GameSwapStatus } from '../interface/game-swap-data';

export class GameSwap extends FrontEndModel implements GameSwapData {
    public gameTitle: string;
    public gameDescription: string;
    public gameConsole: Console;
    public photos: GameSwapPhoto[];
    public gameWished: Game;
    public user: User;
    public status: GameSwapStatus;

    constructor(entity: GameSwapData){
        super(entity);
        this.gameTitle = entity.gameTitle;
        this.gameDescription = entity.gameDescription;
        this.gameConsole = entity.gameConsole;
        this.photos = entity.photos;
        this.gameWished = entity.gameWished;
        this.user = entity.user;
        this.status = entity.status;
    }
}
