import { HttpErrorResponse } from '@angular/common/http';

import { BackEndDataModel } from '@core/interfaces/back-end-data-model';
import { FrontEndModel } from '@core/models/front-end-model';
import { API_BASE, API_HEADERS } from '@env/environment';
import { asyncData, asyncError } from '@tests/helpers';

import { DataAPIService } from './data-api.service';

describe('DataAPIService', () => {
    interface MockInterface extends BackEndDataModel {
        name: string;
        value: string;
    }

    class MockClass  extends FrontEndModel implements MockInterface {
        name: string;
        value: string;

        public constructor(entity: any){
            super(entity);
            this.name = entity.name;
            this.value = entity.value;
        }
    }

    class ConcreteDataAPIService extends DataAPIService<MockClass> {
        public get className() {
            return className;
        }

        // tslint:disable-next-line: no-shadowed-variable
        public modelConstructor<MockInterface>(data: MockInterface): MockClass {
            return new MockClass(data);
        }

        public handleRequestError(errorBody) {
            return Promise.reject(errorBody);
        }

        public beforeSave(data) {
            return Promise.resolve(data);
        }
    }

    let httpClientSpy: {
        get: jasmine.Spy,
        put: jasmine.Spy,
        post: jasmine.Spy
    };
    let service: ConcreteDataAPIService;

    const className = 'SomeClass';
    const APIEndPoint = API_BASE + '/classes/' + className;
    const APIHeaders = API_HEADERS;
    const objectsMock = {
        results: [
            { name: 'a', value: 'val' },
            { name: 'b', value: 'var' },
        ] as MockInterface[],
    };

    beforeEach(() => {
        httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'put', 'post']);
        service = new ConcreteDataAPIService(httpClientSpy as any);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should return expected objects from server', async () => {
        let returnedObjects;

        httpClientSpy.get.and.returnValue(asyncData(objectsMock));

        returnedObjects = await service.getAll();

        returnedObjects.forEach((element, i) => {
            expect(element).toBeInstanceOf(MockClass);
            expect(element).toEqual(jasmine.objectContaining(objectsMock.results[i]));
        });
        expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
        expect(httpClientSpy.get.calls.first().args[0]).toEqual(APIEndPoint);
        expect(httpClientSpy.get.calls.first().args[1]).toEqual({headers: APIHeaders});
    });

    it('should return a specific object when requested by id', async () => {
        let returnedObject;

        const id = 'asdf';
        const endpoint = APIEndPoint + '/' + id;

        httpClientSpy.get.and.returnValue(asyncData(objectsMock.results[0]));

        returnedObject = await service.getOne(id);

        expect(returnedObject).toBeInstanceOf(MockClass);
        expect(returnedObject).toEqual(jasmine.objectContaining(objectsMock.results[0]));
        expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
        expect(httpClientSpy.get.calls.first().args[0]).toEqual(endpoint);
        expect(httpClientSpy.get.calls.first().args[1]).toEqual({headers: APIHeaders});
    });

    it('should save using PUT if the object exists', async () => {
        const rawMock = objectsMock.results[0];

        let returnedObject;
        let existentObject = Object.assign({
            objectId: '1a1a1a1a1a1a1a1a1a1a1a1a'
        }, rawMock) as any;

        existentObject = new MockClass(existentObject);

        const endpoint = APIEndPoint + '/' + existentObject.id;

        httpClientSpy.put.and.returnValue(asyncData(existentObject));

        returnedObject = await service.save(existentObject);

        expect(returnedObject).toEqual(existentObject);
        expect(httpClientSpy.post).toHaveBeenCalledTimes(0);
        expect(httpClientSpy.put).toHaveBeenCalledTimes(1);
        expect(httpClientSpy.put.calls.first().args[0]).toEqual(endpoint);
        expect(httpClientSpy.put.calls.first().args[1]).toEqual(rawMock);
        expect(httpClientSpy.put.calls.first().args[2]).toEqual({headers: APIHeaders});
    });

    it('should save using POST if the object does not exists', async () => {
        let returnedObject;

        const rawMock = objectsMock.results[0];
        const mock = new MockClass(rawMock);
        const endpoint = APIEndPoint;

        httpClientSpy.post.and.returnValue(asyncData(mock));

        returnedObject = await service.save(mock);

        expect(returnedObject).toEqual(mock);
        expect(httpClientSpy.put).toHaveBeenCalledTimes(0);
        expect(httpClientSpy.post).toHaveBeenCalledTimes(1);
        expect(httpClientSpy.post.calls.first().args[0]).toEqual(endpoint);
        expect(httpClientSpy.post.calls.first().args[1]).toEqual(rawMock);
        expect(httpClientSpy.post.calls.first().args[2]).toEqual({headers: APIHeaders});
    });

    it('Should call `handleRequestError` on error from backend', async (done) => {
        let mock = Object.assign({isNew: true}, objectsMock.results[0]) as any;
        mock = new MockClass(mock);
        const endpoint = APIEndPoint;
        const errorResponse = new HttpErrorResponse({
            error: {code: 105, error: 'This is an error!!'},
            status: 500,
            statusText: 'Internal server error'
        });

        const errorHandlerSpy = spyOn(service, 'handleRequestError').and.callThrough();
        httpClientSpy.post.and.returnValue(asyncError(errorResponse));

        try {
            const ret = await service.save(mock as FrontEndModel);
            fail('Should throw an error');
        } catch (error) {
            expect(error).toBe(errorResponse);
            expect(errorHandlerSpy).toHaveBeenCalledTimes(1);
            done();
        }
    });
});
