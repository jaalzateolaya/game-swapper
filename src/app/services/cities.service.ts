import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { City } from '@core/models/city';
import { DataAPIService } from './data-api.service';

@Injectable({
  providedIn: 'root'
})
export class CitiesService extends DataAPIService<City> {
  public get className(): string{
    return 'City';
  }

  /**
   * modelConstructor
   */
  public modelConstructor<CityData>(data) {
    return new City (data);
  }

  public handleRequestError() {
    return Promise.reject('false');
  }

  public beforeSave(data) {
    return Promise.resolve(data);
  }

  public constructor(protected httpClient: HttpClient) {
    super(httpClient);
  }

}
