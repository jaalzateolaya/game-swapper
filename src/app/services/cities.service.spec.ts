import { City } from '@core/models/city';
import { API_BASE, API_HEADERS } from '@env/environment';
import { asyncData } from 'tests/helpers';
import { citiesMock } from 'tests/mocks/modules/cities';

import { CitiesService } from './cities.service';

describe('CitiesService', () => {
  let httpClientSpy: { post: jasmine.Spy, patch: jasmine.Spy, get: jasmine.Spy };
  let service: CitiesService;

  const className = 'City';
  const APIEndpoint = API_BASE + '/classes/' + className;
  const APIHeaders = API_HEADERS;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'patch', 'post']);
    service = new CitiesService(httpClientSpy as any);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should return expected objects from server', async () => {
    const citiesResults = {
      results: citiesMock,
    };

    let returnedObjects;

    httpClientSpy.get.and.returnValue(asyncData(citiesResults));

    returnedObjects = await service.getAll();

    returnedObjects.forEach((element, i) => {
      expect(element).toBeInstanceOf(City);
      expect(element).toEqual(jasmine.objectContaining(citiesMock[i]));
    });
    expect(httpClientSpy.get).toHaveBeenCalledTimes(1);
    expect(httpClientSpy.get.calls.first().args[0]).toEqual(APIEndpoint);
    expect(httpClientSpy.get.calls.first().args[1]).toEqual({headers: APIHeaders});
  });
});
