import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomePage } from './home.page';

const routes: Routes = [
  {
    path: 'home',
    component: HomePage,
    children: [
      {
        path: 'games',
        loadChildren: () => import('@modules/games/pages/games-list/games-list.module')
          .then(m => m.GamesListPageModule)
      },
      {
        path: 'log',
        loadChildren: () => import('@modules/game-swaps/pages/game-swaps-list/game-swaps-list.module')
          .then(m => m.GameSwapsListPageModule)
      },
      {
        path: 'profile',
        loadChildren: () => import('@modules/persons/pages/profile/profile.module')
          .then(m => m.ProfilePageModule)
      },
      {
        path: '',
        redirectTo: '/home/games',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/home/games',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomePageRoutingModule {}
