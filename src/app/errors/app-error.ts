export class AppError extends Error {
    public constructor(...params) {
        super(...params);

        Object.setPrototypeOf (this, new.target.prototype);
        this.name = this.constructor.name;
    }
}
