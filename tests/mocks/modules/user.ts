import { User } from '@core/modules/authentication/models/user';

export const usersMock = [
    {
        username: 'some@email.com'
    },
    {
        username: 'some1@email.com'
    },
    {
        username: 'some2@email.com'
    },
] as any as User[];

export const userMock = usersMock[0];

export const usersAPIResponseMock = usersMock;
