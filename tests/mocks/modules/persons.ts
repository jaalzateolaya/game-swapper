import { Person } from '@core/modules/persons/models/person';
import { cityMock } from './cities';

export const personsMock = [
    {
        forename: 'someName0',
        lastname: 'someLastname0',
        address: 'someAddress0',
        city: cityMock,
        mobile: '1010101010',
    },
    {
        forename: 'someName1',
        lastname: 'someLastname1',
        address: 'someAddress1',
        city: cityMock,
        mobile: '1010101010',
    },
    {
        forename: 'someName2',
        lastname: 'someLastname2',
        address: 'someAddress2',
        city: cityMock,
        mobile: '1010101010',
    },
] as any as Person[];

export const personMock = personsMock[0];

export const personAPIResponseMock = personsMock;
