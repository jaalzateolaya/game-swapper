import { Console } from '@core/modules/consoles/models/console';

export const consolesMock = [
    {
        title: 'Xbox 360',
        image:  'http://localhost/consoleurl',
        color: 1080336
    },
    {
        title: 'Play Station 4',
        image: 'http://localhost/consoleurl',
        color: 14225
    },
    {
        title: 'Nintendo Switch',
        image: 'http://localhost/consoleurl',
        color: 14942223
    },

] as any as Console[];

export const consoleMock = consolesMock[0];

export const consolesAPIResponseMock = consolesMock;
