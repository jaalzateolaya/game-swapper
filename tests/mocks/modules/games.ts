import { consoleMock } from './consoles';
import { Game } from '@core/modules/games/models/game';

export const gamesMock = [
    {
        title: 'Halo Reach',
        image: 'http://localhost/gameurl',
        console: consoleMock,
        get consoleTitle() {
            return this.console.title;
        },
        get consoleColor() {
            return this.console.color;
        },
    },
    {
        title: 'Crash Bandicoot N. Sane Trilogy',
        image: 'http://localhost/gameurl',
        console: consoleMock,
        get consoleTitle() {
            return this.console.title;
        },
        get consoleColor() {
            return this.console.color;
        },
    },
] as any as Game[];

export const gameMock = gamesMock[0];

export const gamesAPIResponseMock = gamesMock;
