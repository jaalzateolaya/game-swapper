import { City } from '@core/models/city';

export const citiesMock = [
    {
        name: 'Pereira',
    },
    {
        name: 'Dosquebradas',
    },
    {
        name: 'Sta Rosa de Cabal',
    },
    {
        name: 'Cartago',
    },
] as any as City[];

export const cityMock = citiesMock[0];
