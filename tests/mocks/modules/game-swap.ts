import { GameSwap } from '@core/modules/game-swaps/models/game-swap';
import { consoleMock } from './consoles';
import { gameMock, gamesMock } from './games';
import { userMock } from './user';

export const gameSwapsMock = [
    {
        gameTitle: 'God of War 3',
        gameDescription: 'Esta meloski',
        gameConsole: consoleMock,
        photos: 'someURLPhoto',
        gameWished: gameMock,
        user: userMock,
        status: 'Pending for mail Validation',
    },
    {
        gameTitle: 'Fifa 19',
        gameDescription: 'Esta meloski',
        gameConsole: consoleMock,
        photos: 'someURLPhoto',
        gameWished: gamesMock[1],
        user: userMock,
        status: 'Approved',
    },
] as any as GameSwap[];

export const gameSwapMock = gameSwapsMock[0];

export const gamesSwapAPIResponseMock = gameSwapsMock;
